use std::error::Error;
use rpncalculator::RpnCalculator;

#[test]
fn test_example_from_site() -> Result<(), Box<dyn Error>> {
    let mut calculator = RpnCalculator::default();
    calculator.push(19);
    calculator.push(2.14);
    calculator.op("+")?;
    calculator.push(4.5);
    calculator.push(2);
    calculator.push(4.3);
    calculator.op("/")?;
    calculator.op("-")?;
    calculator.op("*")?;
    assert!((calculator.top().unwrap() - 85.2974).abs() < 0.0001);
    Ok(())
}